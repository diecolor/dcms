package com.dcms.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: 武佳伟丶
 * \* Date: 2018/1/17 0017
 * \* Time: 16:44
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class TestController {

    @RequestMapping("test")
    public String test(HttpServletRequest request){
        request.setAttribute("test","测试");
        return "test";
    }
}