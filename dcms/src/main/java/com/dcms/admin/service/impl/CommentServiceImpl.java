package com.dcms.admin.service.impl;

import com.dcms.admin.bean.Comment;
import com.dcms.admin.dao.CommentMapper;
import com.dcms.admin.service.ICommentService;
import com.dcms.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * \* @name: CommentServiceImpl
 * \* @author: 武佳伟丶
 * \* @date: 2018/2/11 0011
 * \* @tine: 10:35
 * \* @description: To change this template use File | Settings | File Templates.
 * \
 */
@Service
public class CommentServiceImpl implements ICommentService{

    @Autowired
    private CommentMapper mapper;

    public int deleteByPrimaryKey(Integer commentId) {
        return mapper.deleteByPrimaryKey(commentId);
    }

    public int insertSelective(Comment record) {
        return mapper.insertSelective(record);
    }

    public Comment selectByPrimaryKey(Integer commentId) {
        return mapper.selectByPrimaryKey(commentId);
    }

    public int updateByPrimaryKeySelective(Comment record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @param param
     * @author: 武佳伟丶
     * @description: 根据条件查询所有的comment
     * @date: 10:40 2018/2/11 0011
     * @param: [param] 用于模糊查询content
     * @return: java.util.List<com.dcms.admin.bean.Comment>
     */
    public List<Comment> selectAll(String param) {
        Map map = new HashMap(1);
        if (param != null && !"".equals(param)){
            map.put("param", StringUtil.appendPerCent(param));
        }

        return mapper.selectAll(map);
    }
}