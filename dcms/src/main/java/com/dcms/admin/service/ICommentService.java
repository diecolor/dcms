package com.dcms.admin.service;

import com.dcms.admin.bean.Comment;

import java.util.List;

/**
 * \* @name: ICommentService
 * \* @author: 武佳伟丶
 * \* @date: 2018/2/11 0011
 * \* @tine: 10:35
 * \* @description: To change this template use File | Settings | File Templates.
 * \
 */
public interface ICommentService {
    int deleteByPrimaryKey(Integer commentId);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Integer commentId);

    int updateByPrimaryKeySelective(Comment record);

    /**
     * @author: 武佳伟丶
     * @description: 根据条件查询所有的comment
     * @date: 10:40 2018/2/11 0011
     * @param: [param] 用于模糊查询content
     * @return: java.util.List<com.dcms.admin.bean.Comment>
     */
    List<Comment> selectAll(String param);
}