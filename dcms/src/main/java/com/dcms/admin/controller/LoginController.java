package com.dcms.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * \* @name: LoginController
 * \* @author: 武佳伟丶
 * \* @date: 2018/2/10 0010
 * \* @tine: 19:50
 * \* @description: To change this template use File | Settings | File Templates.
 * \
 */
@Controller
@RequestMapping("admin")
public class LoginController extends BaseController{

    @RequestMapping("sign/in")
    public String toLogin(){
        return "admin/login";
    }
}