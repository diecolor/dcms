package com.dcms.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * \* @name: IndexController
 * \* @author: 武佳伟丶
 * \* @date: 2018/2/10 0010
 * \* @tine: 19:48
 * \* @description: To change this template use File | Settings | File Templates.
 * \
 */
@Controller
@RequestMapping("admin")
public class IndexController extends BaseController{

    @RequestMapping("index")
    public String toIndex(){
        return "admin/index";
    }

}