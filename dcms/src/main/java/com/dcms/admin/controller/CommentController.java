package com.dcms.admin.controller;

import com.dcms.admin.bean.Comment;
import com.dcms.admin.dao.CommentMapper;
import com.dcms.admin.service.ICommentService;
import com.dcms.util.LayTableBean;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * \* @name: CommentController
 * \* @author: 武佳伟丶
 * \* @date: 2018/2/11 0011
 * \* @tine: 10:51
 * \* @description: To change this template use File | Settings | File Templates.
 * \
 */
@Controller
@RequestMapping("admin/comment")
public class CommentController extends BaseController {

    @Autowired
    private ICommentService commentService;

    @RequestMapping("list")
    public String list(){
        return "admin/comment/list";
    }


    /**
     * @author: 武佳伟丶
     * @description: 查询数据
     * @date: 10:55 2018/2/11 0011
     * @param: [request, param, limit, page]
     * @return: com.dcms.util.LayTableBean<com.dcms.admin.bean.Comment>
     */
    @RequestMapping("query")
    @ResponseBody
    public LayTableBean<Comment> query(
            HttpServletRequest request,
            String param,
            Integer limit,
            Integer page
    ){
        page = page == null ? 1 : page;
        limit = limit == null ? 12 : limit;

        PageHelper.startPage(page, limit);
        List<Comment> list = commentService.selectAll(param);
        PageInfo<Comment> info = new PageInfo<Comment>(list);
        LayTableBean<Comment> bean = new LayTableBean<Comment>(info.getList(), (int)info.getTotal());
        return bean;
    }

}