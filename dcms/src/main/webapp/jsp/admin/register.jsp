<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set value="${pageContext.request.contextPath }" var="basePath"></c:set>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>蒙柏信息——用户注册</title>
    <link href="<%=basePath %>plugins/layui/css/layui.css" rel="stylesheet" />
    <link href="<%=basePath %>build/css/login.css" rel="stylesheet" />
    <link href="<%=basePath %>plugins/sideshow/css/demo.css" rel="stylesheet" />
    <link href="<%=basePath %>plugins/sideshow/css/component.css" rel="stylesheet" />
    <link href="<%=basePath %>plugins/sideshow/css/register.css" rel="stylesheet" />
    <link href="<%=basePath %>corporation/css/font.css" media="all" rel="stylesheet">
</head>

<body class="kit-login-bg">
    <div class="container demo-1">
        <div class="content">
            <div id="large-header" class="large-header">
                <div class="kit-login-box">
                    <header>
                        <img src="<%=basePath %>corporation/images/logo.png" />
                    </header>
                    <div class="kit-login-main">
                        <form action="/" class="layui-form" method="post">
                            <div class="layui-form-item">
                                <label class="kit-login-icon">
                                    <i class="layui-icon">&#xe612;</i>
                                </label>
                                <input type="text" name="userName" id="userName" lay-verify="required|checkUserName|username" autocomplete="off" placeholder="用户名" class="layui-input">
                            </div>
                             <div class="layui-form-item">
                                <label class="kit-login-icon">
                                      <i class="layui-icon">&#xe603;</i>
                                </label>
                                <input type="password" name="password" id="password" lay-verify="required|pass" autocomplete="off" placeholder="密码" class="layui-input" maxlength="18">
                            </div>
                             <div class="layui-form-item">
                                <label class="kit-login-icon">
                                      <i class="layui-icon">&#xe603;</i>
                                </label>
                                <input type="password" name="password2" id="password2" lay-verify="required|pass|pass2" autocomplete="off" placeholder="确认密码" class="layui-input">
                            </div>
                            <div class="layui-form-item">
                                <label class="kit-login-icon">
                                    <i class="layui-icon">&#xe63b;</i>
                                </label>
                                <input type="tel" id="phone" name="phone" lay-verify="required|phone|checkPhone" autocomplete="off" placeholder="手机号" class="layui-input">
                            </div>
                            <div class="layui-form-item">
                            	<div class="kit-pull-left">
	                                <label class="kit-login-icon">
	                                    <i class="layui-icon">&#xea01;</i>
	                                </label>
	                                <input style="width: 170px;margin-right: 0px;" type="text" id="validCode" name="validCode" lay-verify="required" autocomplete="off" placeholder="短信验证码" class="layui-input">
                               	</div>
                               	<div class="kit-pull-right">
                               		<div class="layui-btn layui-btn" id="changeCode">获取验证码</div>
                               	</div>
                            </div>
                            <div class="layui-form-item">
                                <button class="layui-btn layui-btn-fluid"  lay-submit lay-filter="login">同意条款并注册</button>
                            </div>
                        </form>
                    </div>
                    <footer>
                       	<a href="#" onclick="agreement()">《蒙柏用户协议》</a>及<a href="#">《蒙柏隐私权保护声明》</a>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->

    <script src="<%=basePath %>plugins/layui/layui.js"></script>
    <script src="<%=basePath %>plugins/sideshow/js/demo-1.js"></script>
    <script src="<%=basePath %>build/js/baseurl.js"></script>
    <script>
    
    	function agreement(){
    	
    			layer.open({
			        type: 1
			        ,title:'《蒙柏用户协议》'
			        ,area: ['400px', '500px']
			        ,offset: 'auto' 
			        ,id: 'layerDemo'+'auto' 
			        ,content: '<form class="layui-form" action="" style="margin:30px">\
					  <div class="layui-form-item">\
					    <div class="layui-row">协议内容！！！！！！！！！！！！！！</div>\
					  </div>\
					</form>'
			        ,shade: 0 //不显示遮罩
			        ,yes: function(){
			          layer.closeAll();
			        }
			      });
    	
    	}
    	
    	//手机正则    
        var myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
        //code，正式发布使用0代替
        var code=0;
        //倒计时
        var wait=60;
        //测试
        var msg = "";
        //
        var phone = "0";
        
        function time() {
			if (wait == 0) {
				$("#changeCode").html("获取验证码");
				wait = 60;
			} else {
				$("#changeCode").html("重新发送(" + wait + ")");
				wait--;
				setTimeout(function() {  
				    time();
			   	},1000);
			}
		}  
    
        layui.use(['layer', 'form'], function() {
            layer = layui.layer,
                $ = layui.jquery,
                form = layui.form;

            $('#changeCode').on('click', function() {
            	msg = "";
                if(!myreg.test($('#phone').val()))
       			{
       				$("#phone").focus();
               		layer.msg('手机号码有误，请重填', {icon: 5});
       			}else {
       				checkPhone($('#phone').val());
                	if(msg!=""){
                	
                	}else{
		                if(wait!=60)
						{
	               			layer.msg('请勿重复发送', {icon: 5}); 
						}else{
							//发送，发送之后执行下一段代码
							checkCode();
						}
                	}	
                }
            });
            
          form.verify({
			  username: function(value){
			    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
			        return '用户名不能有特殊字符';
			    }
			    if(/(^\_)|(\__)|(\_+$)/.test(value)){
			        return '用户名首尾不能出现下划线\'_\'';
			    }
			    if(escape(value).indexOf( "%u" )>=0){
			    	return '用户名不能含有汉字';
			    }
			  }
			  ,pass: [
			    /^[\S]{6,18}$/
			    ,'密码必须6到18位，且不能出现空格'
			  ]
			  ,pass2: function(value){
			  	var pass = $("#password").val();
			  	var pass2 = $("#password2").val();
			  	if(pass!=pass2)
			  	{
			  		return '两次密码不一致';
			  	}
			  }
			  ,checkPhone:function(value){
       			if(!myreg.test($('#phone').val()))
       			{
       				return '手机号码有误，请重填';
       			}
       			//else{
       			//	checkPhone($('#phone').val());
       			//}
			  }
			  ,checkUserName:function(value){
			  	checkUserName($('#userName').val());
			  }
			});
			
			//var checkPhone = false;
			//var checkUserName = false;

          //$('#phone').blur(function(){
	        // checkPhone($(this).val());
          //});
           
          $('#userName').blur(function(){
        	  checkUserName($(this).val());
          });
          
          form.on('submit(login)', function(data) {
          		
          		var newcode = $('#validCode').val();
          		if(newcode!=code||code==0)
          		{
          			$("#validCode").focus();
               		layer.msg('验证码错误', {icon: 5}); 
          		}else if(phone != $('#phone').val()){
          			$("#phone").focus();
               		layer.msg('手机号被修改，请重新获取', {icon: 5}); 
          		}else{
          			//蒙罩层转圈
                    var loadIndex = layer.load(2, {
                        shade: [0.3, '#333']
                    });
          			
          			var mydata=getData(202);
		        	mydata.data[0].usr_name=$('#userName').val();
		        	mydata.data[0].phone=$('#phone').val();
		        	mydata.data[0].passwd=$('#password2').val();
		        	$.ajax({
		            	url:baseurl+'aboutUser/addUser',
		            	type:'post',
		            	contentType:"application/json",
		            	data:JSON.stringify(mydata),
		            	dataType:'json',
		            	success:function(data)
		            	{
		            		if(data.head.result==0)
		            		{
		            			layer.msg('注册成功', {icon: 6}); 
		            			location.href = '${basePath}/user/index';
		            		}else{
		               			layer.msg('注册失败', {icon: 5}); 
		            		}
		            		layer.close(loadIndex);
		            	},
		            	error:function()
		            	{
		            		layer.close(loadIndex);
		            		layer.msg("网络异常，请重试");
		            	}
          			});
          			phone = "0";
          		}
               	return false;
           	});
        });
        
        function checkPhone(phone)
        {
        	var mydata=getData(226);
        	mydata.data[0].phone=phone;
        	$.ajax({
            	url:baseurl+'aboutUser/checkPhone',
            	type:'post',
            	contentType:"application/json",
            	data:JSON.stringify(mydata),
            	dataType:'json',
            	success:function(data)
            	{
            		if(data.data)
            		{
            			//重复了
            			//layer.msg(data.data[0].errinfo);	
            			$("#phone").focus();
	               		layer.msg(data.data[0].errinfo, {icon: 5}); 
	               		msg = data.data[0].errinfo;
            		}	
            		else{
            			//木有正常
            		}
            	},
            	error:function()
            	{
            		layer.close(loadIndex);
            		layer.msg("网络异常，请重试");
            	}
            	
            	
            });
        	
        }
        
        
        function checkUserName(phone)
        {
        	var mydata=getData(201);
        	mydata.data[0].usr_name=phone;
        	$.ajax({
            	url:baseurl+'aboutUser/checkUsrName',
            	type:'post',
            	contentType:"application/json",
            	data:JSON.stringify(mydata),
            	dataType:'json',
            	success:function(data)
            	{
            		if(data.data)
            		{
            			//重复了
            			//layer.msg(data.data[0].errinfo);
	            		$("#userName").focus();
	               		layer.msg(data.data[0].errinfo, {icon: 5}); 
               			//return data.data[0].errinfo;
            		}	
            		else{
            			//木有正常
            		}
            	},
            	error:function()
            	{
            		layer.close(loadIndex);
            		layer.msg("网络异常，请重试");
            	}
            	
            	
            });
        	
        }
        
        function checkCode()
        {
        	phone = $('#phone').val();
        	if(wait==60)
			{
        		$.post('${basePath}/sms/send',{
                	phone: phone
                },function(resp){
                	console.log(resp);
                	if(resp.code==10000){
                		code = resp.object;
                		time();
                	}else{
                		layer.msg(resp.msg, {icon: 5}); 
                	}
                })
			}
        }
        
    </script>
</body>

</html>