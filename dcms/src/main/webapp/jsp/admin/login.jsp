<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set value="${pageContext.request.contextPath }" var="basePath"></c:set>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>用户登陆</title>
    <link href="${basePath}/static/plugins/layui/css/layui.css" rel="stylesheet" />
    <link href="${basePath}/static/build/css/login.css" rel="stylesheet" />
    <link href="${basePath}/static/plugins/sideshow/css/demo.css" rel="stylesheet" />
    <link href="${basePath}/static/plugins/sideshow/css/component.css" rel="stylesheet" />
    <style>
        canvas {
            position: absolute;
            z-index: -1;
        }
        
        .kit-login-box header h1 {
            line-height: normal;
        }
        
        .kit-login-box header {
            height: auto;
            text-align: center;
        }
        
        .content {
            position: relative;
        }
        
        .codrops-demos {
            position: absolute;
            bottom: 0;
            left: 40%;
            z-index: 10;
        }
        
        .codrops-demos a {
            border: 2px solid rgba(242, 242, 242, 0.41);
            color: rgba(255, 255, 255, 0.51);
        }
        
        .kit-pull-right button,
        .kit-login-main .layui-form-item input {
            background-color: transparent;
            /*color: white;*/
        }
        
        .kit-login-main .layui-form-item input::-webkit-input-placeholder {
            color: #CCCCCC;
            font-size: 14px;
        }
        
        .kit-login-main .layui-form-item input:-moz-placeholder {
            color: #CCCCCC;
             font-size: 14px;
        }
        
        .kit-login-main .layui-form-item input::-moz-placeholder {
            color: #CCCCCC;
             font-size: 14px;
        }
        
        .kit-login-main .layui-form-item input:-ms-input-placeholder {
            color: #CCCCCC;
             font-size: 14px;
        }
        
        .kit-pull-right button:hover {
            border-color: #009688;
            color: #009688
        }
        footer{
        	padding: 0 50px 0;
        	font-size: 13px;
        }
        /*
        .form-code{
       	    position: absolute;
		    right: 2px;
		    top: 2px;
		    display: inline-block;
		    text-align: center;
		    line-height: 34px;
		    cursor: pointer;
		    background: #33ABA0;
		    width: 90px;
		    color: #fff;
        }*/
        .form-code:hover{
        	color: #fff;
        }
    </style>
    <script>
    	
    </script>
</head>


<body class="kit-login-bg">
    <div class="container demo-1">
        <div class="content">
            <div id="large-header" class="large-header">
                <div class="kit-login-box">
                    <header>
                          <img src="<%=basePath %>corporation/images/logo.png" />
                    </header>
                    <div class="kit-login-main">
                        <form action="/" class="layui-form" method="post">
                            <div class="layui-form-item">
                                <label class="kit-login-icon">
                                    <i class="layui-icon">&#xe612;</i>
                                </label>
                                <input type="text" id="userName" name="userName" lay-verify="required|username" autocomplete="off" placeholder="用户名" class="layui-input">
                            </div>
                            <div class="layui-form-item">
                                <label class="kit-login-icon">
                                    <i class="layui-icon">&#xe603;</i>
                                </label>
                                <input type="password" id="password" name="password" lay-verify="required|pass" autocomplete="off" placeholder="密码" class="layui-input">
                            </div>
                            
                             <div class="layui-form-item">
                                <label class="kit-login-icon">
                                    <i class="layui-icon">&#xea01;</i>
                                </label>
                                <input type="text" id="validCode" name="validCode" lay-verify="required|validCode" autocomplete="off" placeholder="输入验证码" class="layui-input">
                            </div>
                            <div class="layui-form-item">
                            	<div class="kit-pull-right">
                            		<a href="${basePath }/user/reg" target="_Blank" style="margin-right: 10px;">立即注册</a>
                            		<a href="${basePath }/user/forget" target="_Blank">忘记密码</a>
                            	</div>
                        	</div>
                        </form>
                    </div>
                    <footer>
                        <p>** 版权所有</p>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->

    <script src="${basePath}/static/plugins/layui/layui.js"></script>
    <script src="${basePath}/static/plugins/sideshow/js/demo-1.js"></script>
    <script src="${basePath}/static/build/js/baseurl.js"></script>
    
    <script src="${basePath}/static/plugins/sideshow/js/TweenLite.min.js"></script>
    <script src="${basePath}/static/plugins/sideshow/js/EasePack.min.js"></script>
    <script src="${basePath}/static/plugins/sideshow/js/rAF.js"></script>
    <script src="${basePath}/static/plugins/sideshow/js/demo-1.js"></script>
    <script>
    
    	
    	var code=0;
    	var layer,$,form;
    	
    	function loadImg()
    	{
    		$.ajax({
    			url:'<%=basePath%>admin/code/getcode',
    			dataType:'json',
    			success:function(data)
    			{
    				//alert(data.object)
    				if(data.code=="10000")
    				{
    					$('#refImg').attr('src','data:image/png;base64,'+data.object);
    					code=data.msg;
    					//$('#validCode').val(code);
    				}
    				else
    				{
    					layer.msg("验证码加载失败");
    				}	
    			}
    		});
    	}
    	
        layui.use(['layer', 'form'], function() {
            layer = layui.layer,
                $ = layui.jquery,
                form = layui.form;
			
            loadImg();//首次加载
			$('#refImg').click(function(){
				 loadImg()
				
			});
            form.verify({
			  username: function(value){
			    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
			        return '用户名不能有特殊字符';
			    }
			    if(/(^\_)|(\__)|(\_+$)/.test(value)){
			        return '用户名首尾不能出现下划线\'_\'';
			    }
			    if(escape(value).indexOf( "%u" )>=0){
			    	return '用户名不能含有汉字';
			    }
			  },pass: [
			    /^[\S]{6,18}$/
			    ,'密码必须6到18位，且不能出现空格'
			  ],validCode: function(value){
				  if(value+''!= code+''){
					  return '验证码错误';
				  }
				   
			  }
			});

            $(window).on('load', function() {
                form.on('submit(login)', function(data) {
                   	var newcode = $("#validCode").val();
	                if(newcode!=code)//||code==0
	          		{
	          			$("#validCode").focus();
	               		layer.msg('验证码错误', {icon: 5}); 
	          		}else{
	          			//蒙罩层转圈
	                    var loadIndex = layer.load(2, {
	                        shade: [0.3, '#333']
	                    });
	          		
	                    var mydata=getData(203);
			        	mydata.data[0].usr_name=$('#userName').val();
			        	mydata.data[0].passwd=$('#password').val();
			        	$.ajax({
			            	url:baseurl+'aboutUser/login',
			            	type:'post',
			            	contentType:"application/json",
			            	data:JSON.stringify(mydata),
			            	dataType:'json',
			            	success:function(data)
			            	{
			            		if(data.data[0].errno)
			            		{
			            			layer.msg(data.data[0].errinfo, {icon: 5}); 
			            		}else{
			            			//layer.msg(data.data[0].phone, {icon: 6}); 
			            			//console.log(data.data[0])
			            			setUser(data.data[0], function(){
			            				location.href = '${basePath}/user/index';
			            			});
			            		}
			            		layer.close(loadIndex);
			            	},
			            	error:function()
			            	{
			            		layer.close(loadIndex);
			            		layer.msg("网络异常，请重试");
			            	}
	          			});
          			}
                    return false;
                });
            }());

        });
        
        function sendCode(){
        	var phone = $("#userName").val();
            $.post('${basePath}/sms/send',{
            	phone: phone
            },function(resp){
            	console.log(resp);
            	if(resp.code==10000){
            		code = resp.object;
            		var num = 60;
            		$('#changeCode').html(num + 's').removeAttr("onclick");
            		var interval = window.setInterval(function(){
            			num --;
            			$('#changeCode').html(num + 's');
            		},1000);
            		//shut down
            		window.setTimeout(function(){
            			clearInterval(interval);
            			$('#changeCode').html('获取验证码').attr('onclick', "sendCode()");
            		}, 59000);
            	}else{
            		layer.msg(resp.msg, {icon: 5}); 
            	}
            })
        }
    </script>
</body>

</html>