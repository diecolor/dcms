<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="basePath" value="${pageContext.request.contextPath}"/>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>留言列表</title>
	<link href="${basePath }/static/plugins/layui/css/layui.css" rel="stylesheet"/>
</head>
<body>
<div id="main">
	<div class="layui-btn-group demoTable">
	  <button class="layui-btn" data-type="getCheckData">删除</button>
	</div>
	<div id="table" lay-filter="table"></div>
</div>

<script src="${basePath }/static/plugins/layui/layui.js"></script>
<script>

	layui.use(['table', 'layer'], function(){
		table = layui.table,
		layer = layui.layer;
		
		tableIns = table.render({
		    elem: '#table'
		    ,height: 'full'
		    ,url: '${basePath}/admin/comment/query' //数据接口
		    ,page: true //开启分页
		    ,limit: 12
		    ,cols: [[ //表头
              { type: 'checkbox', width: 50}      
		      ,{field: 'commentId', title: 'ID', sort: true}
		      ,{field: 'content', title: '内容', edit: 'text'}
		      ,{field: 'email', title: '邮箱',  sort: true, edit: 'text'}
		      ,{field: 'city', title: '城市', } 
		      ,{field: 'sign', title: '签名', edit: 'text'}
		      ,{field: 'experience', title: '积分', sort: true}
		      ,{field: 'score', title: '评分', sort: true}
		      ,{field: 'classify', title: '职业'}
		      ,{field: 'wealth', title: '财富', sort: true}
		    ]]
	  	});
		
		//监听单元格编辑
	  	table.on('edit(table)', function(obj){
		    var value = obj.value //得到修改后的值
		    ,data = obj.data //得到所在行所有键值
		    ,field = obj.field; //得到字段
		    layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改为：'+ value);
	  	});
	  	//监听表格复选框选择
	    table.on('checkbox(table)', function(obj){
	      console.log(obj)
	    });
	  	//监听工具条
	    table.on('tool(demo)', function(obj){
	      var data = obj.data;
	      if(obj.event === 'detail'){
	        layer.msg('ID：'+ data.id + ' 的查看操作');
	      } else if(obj.event === 'del'){
	        layer.confirm('真的删除行么', function(index){
	          obj.del();
	          layer.close(index);
	        });
	      } else if(obj.event === 'edit'){
	        layer.alert('编辑行：<br>'+ JSON.stringify(data))
	      }
	    });
	  	
	    var $ = layui.$, active = {
	   	    getCheckData: function(){ //获取选中数据
	   	      var checkStatus = table.checkStatus('table')
	   	      ,data = checkStatus.data;
	   	      layer.alert(JSON.stringify(data));
	   	    }
	   	  };
	   	  
	   	  $('.demoTable .layui-btn').on('click', function(){
	   	    var type = $(this).data('type');
	   	    active[type] ? active[type].call(this) : '';
	   	  });
	});

	function detailFormatter(index, row) {
		var html = [];
		$.each(row, function (key, value) {
			html.push('<p><b>' + key + ':</b> ' + value + '</p>');
		});
		return html.join('');
	}

	// 新增
	function insertAction() {
		bangle.insert('测试标题',$('#createDialog').html(),function(){
			$.alert('确认');
			console.log('点击了确认按钮')
		})
	}
	// 编辑
	function updateAction(item) {
		var index = layer.open({
            type: 2,
            shade: 0.3,
            title: '编辑',
            content: '${basePath}/user/edit?id=' + item.id,
            btn: ['确认', '取消'],
            area: ['80%','90%'],
            yes: function(index, layero){
                bangle.load();
                var body = layer.getChildFrame('body', index);
                var $form = $(body.find('#form'));
                console.log($form);
                var data = new FormData($form[0]);
                $.ajax({
                    url: '${basePath}/user/update',
                    data: data,
                    dataType: 'json',
                    cache: false,
                    async: true,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    success: function (resp) {
                        console.log(resp);
                        layer.closeAll('loading');
                        if(resp.code==0){
                            bangle.toast('录入成功', function () {
								layer.close(index);
                                $table.jsGrid("loadData");
                            });
                        }else{
                            bangle.toast(resp.msg);
                        }
                    },
                    fail: function () {
                        bangle.toast('出现异常');
                    }
                })
            },
            btn2: function(index, layero){
                layer.close(index);
            }
		});
	}
	// 删除
	function deleteAction(value) {
		bangle.delete('确认删除吗？',function(){
			$.post('${basePath}/user/delete',{
			    id: value
			}, function (resp) {
				bangle.toast(resp.msg, function () {
                    layer.closeAll();
                    $table.jsGrid("loadData");
                });

            })
		})
	}

</script>
</body>
</html>