<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set value="${pageContext.request.contextPath }" var="basePath"></c:set>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>DCMS</title>
    <link rel="stylesheet" href="<%=basePath %>static/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="<%=basePath %>static/plugins/font-awesome/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="<%=basePath %>static/plugins/corporation/css/app.css" media="all">
    <link rel="stylesheet" href="<%=basePath %>static/plugins/corporation/css/font.css" media="all">
    <style>
    	.layui-nav .layui-badge, .layui-nav .layui-badge-dot
    	{
    		top: 45%;
    	}
    </style>
</head>

<body>
    <div class="layui-layout layui-layout-admin kit-layout-admin">
        <div class="layui-header">
            <div class="layui-logo">
                <img src="<%=basePath %>corporation/images/logo2.png">
            </div>
            <ul class="layui-nav layui-layout-left kit-nav">
                <li class="layui-nav-item"><a href="javascript:" data-url="main" data-icon="fa-user" data-title="首页" kit-target data-id='-1'>首页</a></li>
                <li class="layui-nav-item"><a href="javascript:">产品中心</a>
                    <dl class="layui-nav-child">
                        <dd><a onclick="robot(1)">USB机器人</a></dd>
                        <dd><a onclick="robot(2)">USB财税云</a></dd>
                    </dl>
                </li>
            </ul>
            <ul class="layui-nav layui-layout-right kit-nav">
                <li class="layui-nav-item" style="width: 80px;"><a href="javascript:" id="massage" data-method="msg"> 消息 </a></li>
                <li class="layui-nav-item"><a href="javascript:" ><i class="layui-icon" style="font-size: 13px;" aria-hidden="true">&#xe607;</i> 帮助</a></li>
            </ul>
        </div>

        <div class="layui-side layui-bg-black kit-side">
            <div class="layui-side-scroll">
                <div class="kit-side-fold"><i class="fa fa-navicon" aria-hidden="true"></i></div>
                <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                <ul class="layui-nav layui-nav-tree" lay-filter="kitNavbar" kit-navbar>
                    <li class="layui-nav-item">
                        <a class="" href="javascript:"><i class="layui-icon" aria-hidden="true">&#xe612;</i><span> 账号中心</span></a>
                        <dl class="layui-nav-child">
                            <dd>
                                <a href="javascript:" kit-target data-options="{url:'data',icon:'&#xe60b;',title:'账号资料',id:'1'}">
                                    <i class="layui-icon">&#xe60b;</i><span> 账号信息</span></a>
                            </dd>
                            <dd>
                                <a href="javascript:" kit-target data-options="{url:'safe',icon:'&#xe62c;',title:'安全设置',id:'2'}">
                                    <i class="layui-icon" aria-hidden="true">&#xe62c;</i><span> 安全设置</span></a>
                            </dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item">
                        <a href="javascript:"><i class="layui-icon" aria-hidden="true">&#xe724;</i><span> 留言管理</span></a>
                        <dl class="layui-nav-child">
                            <dd><a href="javascript:" kit-target data-options="{url:'${basePath }/admin/comment/list',icon:'&#xe658;',title:'查看留言',id:'6'}"><i class="layui-icon">&#xe658;</i><span> 查看留言</span></a></dd>
                        </dl>
                    </li>
                    <li class="layui-nav-item">
                        <a href="javascript:" kit-target data-options="{url:'${basePath }/terminus/my',icon:'&#xe6b3;',title:'我的电脑',id:'9'}" kit-loader><i class="layui-icon" aria-hidden="true">&#xe6b3;</i><span> 我的电脑</span></a>
                    </li>
                    <li class="layui-nav-item" id="menu-cmy">
                        <a href="javascript:"><i class="layui-icon" aria-hidden="true">&#xea00;</i><span> 我的终端</span></a>
                        <dl class="layui-nav-child">

                        </dl>
                    </li>
                </ul>
            </div>
        </div>
        <div class="layui-body" id="container">
            <!-- 内容主体区域 -->
            <div style="padding: 15px;">加载中,请稍等...</div>
        </div>

        <div class="layui-footer">
            DCMS 版权所有
        </div>
    </div>
    <script src="<%=basePath %>static/plugins/layui/layui.js"></script>
    <script src="<%=basePath %>static/plugins/jq/jquery-2.2.4.min.js"></script>
    <script>
     	layui.config({
            base: '<%=basePath %>static/build/js/'
        }).use(['app', 'message', 'tab'], function() {
            app = layui.app,
                $ = layui.jquery,
                layer = layui.layer,
                tab = layui.tab;

            app.init();

        });

    </script>
</body>

</html>